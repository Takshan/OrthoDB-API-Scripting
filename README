This project aims to collect examples of scripts that can be used as templates
for building queries to retreive data from OrthoDB using the dedicated 
Application Programming Interface (API).

These templates should provide a useful starting point for users to build their
own specific queries based on the examples provided.

For further details on using the API visit: http://www.orthodb.org/?page=api

If you use any of these templates in your research please cite:
OrthoDB v9.1: cataloging evolutionary and functional annotations for animal,
fungal, plant, archaeal, bacterial and viral orthologs. Nucleic Acids Res. 2017
Zdobnov EM, Tegenfeldt F, Kuznetsov D, Waterhouse RM, Simão FA, Ioannidis P,
Seppey M, Loetscher A, Kriventseva EV. Jan 4;45(D1):D744-D749.
doi: 10.1093/nar/gkw1119. Epub 2016 Nov 28. PMID: 27899580
And in your methods section please point to this GitLab project page.


Changes implemented during the update from OrthoDB v9 to v10 mean a few changes in how to build queries.  

The URL for v10 needs to be https (previously http).  

The species taxonomy identifiers in v10 need an '_0' appended to them.  

Using https://www.orthodb.org/ should connect to the 'current' OrthoDB.  
To select a specific version:  
https://www.orthodb.org/v10/  
https://www.orthodb.org/v9.1/  
https://www.orthodb.org/v9/   

